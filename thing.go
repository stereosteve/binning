package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
)

var (
	debug    = false
	binCount = 12
)

func main() {
	nums := readNums()
	sort.Float64s(nums)

	best := buildBins(nums)
	bestScore := score(best)

	for run := 0; run < 600; run++ {
		b := buildBins(nums)
		s := score(b)
		if s < bestScore {
			best = b
			bestScore = s
		}
	}

	print(best)
	// fmt.Println(bestScore)

}

func readNums() []float64 {
	file, err := os.Open(os.Args[1])
	checkErr(err)
	defer file.Close()

	nums := []float64{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		num, err := strconv.ParseFloat(scanner.Text(), 64)
		checkErr(err)
		nums = append(nums, num)
	}

	return nums
}

func buildBins(nums []float64) [][]float64 {
	bins := make([][]float64, binCount)
	var order []int
	for i, n := range nums {
		if i%12 == 0 {
			order = rand.Perm(binCount)
		}
		idx := order[i%12]
		bins[idx] = append(bins[idx], n)
	}
	return bins
}

func score(bins [][]float64) float64 {
	tally := make([]float64, 12)
	for i, bin := range bins {
		for _, n := range bin {
			tally[i] += n
		}
	}

	sort.Float64s(tally)
	score := tally[11] - tally[0]

	return score
}

func print(bins [][]float64) {
	out := os.Stdout
	for _, bin := range bins {
		fmt.Fprintf(out, "b ")
		for _, n := range bin {
			fmt.Fprintf(out, "%g ", n)
		}
		fmt.Fprintf(out, "\n")

	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
