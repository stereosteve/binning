#! /usr/bin/env python
import sys
import random
import time


def output(bins):
    for bin in bins:
        sys.stdout.write('b ')
        for n in bin:
            sys.stdout.write(str(n))
            sys.stdout.write(' ')
        sys.stdout.write('\n')
    sys.stdout.flush()


def best_fit(numbers):
    bins =  [[] for _ in range(12)]
    sizes = [0] * 12

    for number in numbers:
        if random.random() < 0.1:
            idx = random.randint(0, 11)
        else:
            idx = sizes.index(min(sizes))
        bin = bins[idx]
        bin.append(number)
        sizes[idx] += number

    score = max(sizes) - min(sizes)

    return bins, score


if __name__ == "__main__":
    numbers = [float(line) for line in open(sys.argv[1])]

    numbers.sort(reverse=True)
    bins = None
    bs = 9999999

    i = 0
    start_time = time.time()

    while True:

        (b, s) = best_fit(numbers)
        if s < bs:
            bins = b
            bs = s

        if i % 5000 == 0:
            spent = time.time() - start_time
            if spent > 9.0:
                break

        i += 1

    output(bins)
    if len(sys.argv) > 2:
        print bs
