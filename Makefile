run:
	go run thing.go sample_numbers.txt debug

push:
	docker build -t stereosteve/binning:v3 .
	docker push stereosteve/binning:v3
